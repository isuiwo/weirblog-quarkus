package com.weir.quarku.tanent;

import io.quarkus.arc.Unremovable;
import io.quarkus.hibernate.orm.runtime.tenant.TenantResolver;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.Optional;

/**
 * 识别动态租户信息(一般通过web的request从header传递租户信息)
 * @author weir
 *
 */
@RequestScoped
@Unremovable
public class InjectableTenantResolver implements TenantResolver {

    @Inject
    TenantConnections tenantConnections;

    private Optional<String> requestTenant = Optional.empty();

    public void setRequestTenant(String requestTenant) {
        this.requestTenant = Optional.of(requestTenant);
    }
    /**
     * 默认租户
     */
    @Override
    public String getDefaultTenantId() {
        return tenantConnections.allTenants()
                .stream()
                .findAny()
                .orElseThrow(() -> new RuntimeException("No tenants known at all"));
    }
    /**
     * 当前租户
     */
    @Override
    public String resolveTenantId() {
        return requestTenant.orElseThrow(() -> new RuntimeException("No tenant specified by current request"));
    }
}
