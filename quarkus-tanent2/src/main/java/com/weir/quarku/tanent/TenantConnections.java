package com.weir.quarku.tanent;

import io.agroal.api.AgroalDataSource;
import io.agroal.api.configuration.AgroalDataSourceConfiguration;
import io.agroal.api.configuration.supplier.AgroalDataSourceConfigurationSupplier;
import io.agroal.api.security.NamePrincipal;
import io.agroal.api.security.SimplePassword;
import io.quarkus.arc.Unremovable;
import io.quarkus.hibernate.orm.runtime.customized.QuarkusConnectionProvider;
import io.quarkus.hibernate.orm.runtime.tenant.TenantConnectionResolver;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static io.agroal.api.configuration.AgroalConnectionPoolConfiguration.ConnectionValidator.defaultValidator;
import static java.time.Duration.ofSeconds;

/**
 * 动态产生并切换数据源连接
 * @author weir
 *
 */
//@PersistenceUnitExtension
@ApplicationScoped
@Unremovable
public class TenantConnections implements TenantConnectionResolver {

	private final Map<String, DBConnectionInfo> dbConnectionInfoMap = new HashMap<>();
//    {
//    	{
//    		put("weir",new DBConnectionInfo("localhost", 3306, "root", "336393", "quarkus-demo"));
//    		put("weir-blog",new DBConnectionInfo("localhost", 3306, "root", "336393", "weirblog"));
//    	}
//    };

	private final Map<String, ConnectionProvider> cache = new HashMap<>();

	private static AgroalDataSourceConfiguration createDataSourceConfiguration(DBConnectionInfo dbConnectionInfo) {
		System.out.println("------------------createDataSourceConfiguration--------------" + dbConnectionInfo);
		return new AgroalDataSourceConfigurationSupplier()
				.dataSourceImplementation(AgroalDataSourceConfiguration.DataSourceImplementation.AGROAL)
				.metricsEnabled(false)
				.connectionPoolConfiguration(cp -> cp.minSize(0).maxSize(5).initialSize(0)
						.connectionValidator(defaultValidator()).acquisitionTimeout(ofSeconds(5))
						.leakTimeout(ofSeconds(5)).validationTimeout(ofSeconds(50)).reapTimeout(ofSeconds(500))
						.connectionFactoryConfiguration(cf -> cf
								.jdbcUrl("jdbc:mysql://" + dbConnectionInfo.getHost() + ":" + dbConnectionInfo.getPort()
										+ "/" + dbConnectionInfo.getDb())
								.connectionProviderClassName("com.mysql.cj.jdbc.Driver")
//                                .connectionProviderClassName("org.postgresql.Driver")
								.principal(new NamePrincipal(dbConnectionInfo.getUser()))
								.credential(new SimplePassword(dbConnectionInfo.getPassword()))))
				.get();
	}

	public Set<String> allTenants() {
		getTenant();
		System.out.println("---------------TenantConnections--------allTenants-------" + dbConnectionInfoMap.keySet());
		return dbConnectionInfoMap.keySet();
	}

	@Inject
	AgroalDataSource defaultDataSource;

	private void getTenant() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = defaultDataSource.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select * from SysTenant");

			while (resultSet.next()) {
				dbConnectionInfoMap.put(resultSet.getString("code"),
						new DBConnectionInfo(resultSet.getString("host"), resultSet.getInt("port"),
								resultSet.getString("username"), resultSet.getString("password"),
								resultSet.getString("dbName")));
//				System.out.println("--------------------------"+resultSet.getString("name")+"--"+resultSet.getString("username"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public ConnectionProvider resolve(String tenant) {
		System.out.println("---------------TenantConnections--------resolve-------" + tenant);
		if (!dbConnectionInfoMap.containsKey(tenant)) {
			throw new IllegalStateException("Unknown tenantId: " + tenant);
		}

		if (!cache.containsKey(tenant)) {
			try {
				DBConnectionInfo dbConnectionInfo = dbConnectionInfoMap.get(tenant);
				AgroalDataSource agroalDataSource = AgroalDataSource
						.from(createDataSourceConfiguration(dbConnectionInfo));
				QuarkusConnectionProvider quarkusConnectionProvider = new QuarkusConnectionProvider(agroalDataSource);
				cache.put(tenant, quarkusConnectionProvider);
				return quarkusConnectionProvider;
			} catch (SQLException ex) {
				throw new IllegalStateException("Failed to create a new data source based on the tenantId: " + tenant,
						ex);
			}
		}
		return cache.get(tenant);
	}
}
