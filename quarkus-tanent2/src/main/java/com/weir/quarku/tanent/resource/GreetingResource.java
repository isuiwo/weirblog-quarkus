package com.weir.quarku.tanent.resource;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.weir.quarku.tanent.entity.Gift;


@Path("gift")
@ApplicationScoped
public class GreetingResource {

	@GET
	@Path("{id}")
	public Gift get(@PathParam("id") Integer id) {
		return Gift.findById(id);
	}
   
}

