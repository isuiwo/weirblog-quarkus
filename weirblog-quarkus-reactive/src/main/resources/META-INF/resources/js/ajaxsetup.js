let requestSuccessCode = 200;  // 表示请求成功
let tokenName = "accountToken";
$.ajaxSetup({
    // ajax请求之前进行accountToken封装
    beforeSend: function (xhr) {
        if(accountToken && accountToken != '') {
            xhr.setRequestHeader(tokenName, accountToken);
        }
    },
    // ajax 请求完成返回结果
    complete : function(request) {
        if(request.status == 200) {
            let responseCode = request.responseJSON.code;
            if(responseCode == 6001) {
                clearUserCookie();
                let url = window.location.href;
                redirectLogin(url);
            } else if (responseCode == 6002) {
                window.open("/index.html");
            }
        }
    },
    // 表示请求错误
    error :function(request){
        console.info(request);
    }
});