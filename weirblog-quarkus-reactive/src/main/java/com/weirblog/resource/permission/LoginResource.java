package com.weirblog.resource.permission;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.hibernate.reactive.mutiny.Mutiny;

import com.weirblog.entity.Tmenu;
import com.weirblog.entity.Users;
import com.weirblog.vo.LoginUserVo;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * 登录生成token
 * 
 * @author weir
 *
 */
@Path("/")
@ApplicationScoped
public class LoginResource {

	@Inject
	TokenService tokenService;
	
	@Inject
	Mutiny.Session session;

	@PermitAll
	@POST
	@Path("login")
	public Uni<Response> login(@RequestBody LoginUserVo user) {
		return Panache.withTransaction(()->Users.<Users>find("userName", user.username).singleResult().onItem().ifNotNull()
				.invoke(u->{
					session.createNativeQuery("select m.* from Tmenu m INNER JOIN perm_role_permission rm on m.id = rm.permission_id "
							+ "INNER JOIN perm_user_role ur on rm.role_id=ur.role_id where ur.user_id = ?", Tmenu.class)
					.setParameter(1, u.id).getResultList()
					.onItem().ifNotNull().invoke(modules->{
						Set<String> codes = modules.stream().map(Tmenu::getCode).collect(Collectors.toSet());
						u.moduleCodes = codes;
						String ujson = new JsonObject().put("user", u).toString();
						String token = tokenService.generateToken(u.email, ujson, codes);
						u.token = token;
					});
				})
				
				).onItem().ifNotNull()
		.transform(entity -> Response.ok(new JsonObject()
				.put("msg", "修改成功")
				.put("success", true)
				.put("data", entity)).build())
		.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
		
		
//		Uni<Users> u = Users.find("userName", user.username).firstResult();
//		if (u == null) {
//			return Response.status(Status.BAD_REQUEST).entity(
//					new JsonObject().put("message", "用户不存在")).build();
//		}
//		List<Tmenu> modules = 
//				Tmenu.getEntityManager().createNativeQuery("select m.* from Tmenu m INNER JOIN perm_role_permission rm on m.id = rm.permission_id "
//						+ "INNER JOIN perm_user_role ur on rm.role_id=ur.role_id where ur.user_id = ?", Tmenu.class)
//				.setParameter(1, u.id).getResultList();
//		if (!user.password.equals(u.userPwd)) {
//			return Response.status(Status.BAD_REQUEST).entity(new JsonObject().put("message", "用户名或密码错误")).build();
//		}
//		Set<String> codes = modules.stream().map(Tmenu::getCode).collect(Collectors.toSet());
//		u.moduleCodes = codes;
//		String ujson = new JsonObject().put("user", u).toString();
//		String token = tokenService.generateToken(u.email, ujson, codes);
//		u.token = token;
//		return Response.ok().entity(
//				new JsonObject()
//				.put("data", u)).build();
	}
	
}
