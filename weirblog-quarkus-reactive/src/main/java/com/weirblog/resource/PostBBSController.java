package com.weirblog.resource;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import com.weirblog.entity.PostBbs;
import com.weirblog.entity.Posts;
import com.weirblog.entity.Users;
import com.weirblog.util.BaseUtil;
import com.weirblog.util.ImgTools;
import com.weirblog.util.IpUtil;
import com.weirblog.util.JWTUtil;
import com.weirblog.vo.DataGrid;
import com.weirblog.vo.FileBody;
import com.weirblog.vo.IndexVo;
import com.weirblog.vo.page.PageView;
import com.weirblog.vo.page.QueryResult;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import io.quarkus.panache.common.Sort.Direction;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

/**
 * 文章管理
 * 
 * @author Administrator
 *
 */
@Path("posts_bbs")
@ApplicationScoped
public class PostBBSController {
	@ConfigProperty(name = "photo.img.bbs.save-path")
	String bbsImgPath;
	@ConfigProperty(name = "photo.img.bbs.url-path")
	String bbsImgUrl;

	@Inject
	Mutiny.Session session;
	@Inject
	Mutiny.SessionFactory sessionFactory;
	
	@GET
	@Path("/get-types")
	public Response getTypes() {
		String singleResult = "";
//				session.createNativeQuery("select GROUP_CONCAT(types) from Posts").getSingleResult()
//				.toString();
		List<String> split = stringSplit(singleResult);
		Map<String, Long> map = split.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		return Response.status(Status.OK).entity(new JsonObject().put("map", map)).build();
	}

	public static List<String> stringSplit(String str) {
		List<String> lis = new ArrayList<>();
		String[] split = str.split(",");
		for (int i = 0; i < split.length; i++) {
			String url = split[i];
			String[] split2 = url.split("，");
			if (split2 != null && split2.length > 1) {
				for (int i2 = 0; i2 < split2.length; i2++) {
					lis.add(split2[i2]);
				}
			} else {
				lis.add(url);
			}
		}
		return lis;
	}

	@GET
	@Path("pnum/{id}")
	@Transactional
	public Uni<Response> pnum(@PathParam("id") Integer id) {
		return Panache.withTransaction(() -> PostBbs.<PostBbs>findById(id).onItem().ifNotNull().invoke(entity -> {
			entity.viewCount = entity.viewCount + 1;
		})).onItem().ifNotNull()
				.transform(entity -> Response.ok(new JsonObject().put("msg", "修改成功").put("success", true)).build())
				.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
	}
	@POST
	@Path("add")
	public Uni<Response> add(
			PostBbs postBbs,
			@CookieParam("weir-cookie-toekn") String token) {
		String username = JWTUtil.getUsername(token);
		return Users.find("token", username).singleResult().chain(u->{
			postBbs.user = (Users) u;
			return Panache.withTransaction(postBbs::persist);
		}).replaceWith(
				Response.ok(new JsonObject().put("msg", "新增成功").put("success", true)).status(Status.OK)::build
				);
//		Users users = (Users) Users.find("token", username).singleResult().await().indefinitely();
//		PostBbs posts = new PostBbs();
//		posts.title = title;
//		posts.tag = tag;
//		posts.content = content;
//		posts.id = id;
//		postBbs.user = users;
//		return Panache.withTransaction(postBbs::persist).replaceWith(
////				Response.ok(new JsonObject().put("msg", "新增成功").put("success", true)).status(Status.CREATED)::build
//				Response.seeOther(UriBuilder.fromPath("/bbs").build()).build()
//				);
	}

	@POST
	@Path("edit")
	public Uni<Response> edit(Posts posts) {
		return Panache.withTransaction(() -> Posts.<Posts>findById(posts.id).onItem().ifNotNull().invoke(entity -> {
			entity.postTitle = posts.postTitle;
			entity.types = posts.types;
			entity.description = posts.description;
			entity.content = posts.content;
		})).onItem().ifNotNull()
				.transform(entity -> Response.ok(new JsonObject().put("msg", "修改成功").put("success", true)).build())
				.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
	}

	@GET
	@Path("list")
	public Uni<Object> list(@QueryParam("page") Integer page, @QueryParam("rows") Integer rows) {
		return Panache.withTransaction(() -> {
			PanacheQuery<Posts> findAll = Posts.findAll(Sort.by("createDate", Direction.Descending));
			Uni<Long> count = findAll.count();
			Uni<List<Posts>> list = findAll.page(Page.of(page - 1, rows)).list();
			return Uni.combine().all().unis(count, list).combinedWith((c, l) -> new DataGrid<Posts>(c, l));
		});
	}

}
