package com.weirblog.resource;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

//import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import com.weirblog.util.BaseUtil;
import com.weirblog.util.ImgTools;
import com.weirblog.vo.FileBody;
import com.weirblog.vo.FileMdBody;

import io.vertx.core.http.HttpServerRequest;

@ApplicationScoped
@Path("kindeditor")
public class KindEditorController {

	@ConfigProperty(name = "photo.img.bbs.save-path")
	String bbsImgPath;
	@ConfigProperty(name = "photo.img.save-path")
	String photoImgPath;
	@ConfigProperty(name = "photo.img.url-path")
	String photoImgUrl;
	@ConfigProperty(name = "photo.img.bbs.url-path")
	String bbsImgUrl;
	@Context
	HttpServerRequest request;
	
	// 定义允许上传的文件扩展名
//	private static Map<String, String> extMap = new HashMap<>(){{
//		extMap.put("image", "gif,jpg,jpeg,png,bmp");
//		extMap.put("flash", "swf,flv");
//		extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb,mp4,mkv");
//		extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");
//		extMap.put("audio", "mp3");
//	}};

	@POST
	@Path("file-upload-bbs/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Object> fileUploadBBS(@PathParam("id") Integer id,
			@MultipartForm FileMdBody fileBody) {
		File file = new File(bbsImgPath + id);
		file.mkdirs();
		String newFileName = null;
		try {
			InputStream i = fileBody.inputStream.getBody(InputStream.class, null);
			String[] contentDisposition = fileBody.inputStream.getHeaders().getFirst("Content-Disposition").split(";");
			String fileName = "";
			for (String filename : contentDisposition) {
				if ((filename.trim().startsWith("filename"))) {
					String[] name = filename.split("=");
					fileName = name[1].trim().replaceAll("\"", "");
				}
			}
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			newFileName = BaseUtil.parseDateToStringNomm(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
//			IOUtils.copy(i, new FileOutputStream(new File(photoImgPath + id + "/" + newFileName)));
			
			String srcImageFile = bbsImgPath + id + "/" + fileName;
			Files.copy(i, Paths.get(srcImageFile),StandardCopyOption.REPLACE_EXISTING);
			String newFile = bbsImgPath + id + "/" + newFileName;
			ImgTools.thumbnail_w_h(srcImageFile, 500, 300, newFile);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Map<String, Object> msg = new HashMap<String, Object>();
		msg.put("success", 1);
		msg.put("message", "ok");
		msg.put("url", bbsImgUrl + id + "/" + newFileName);
		return msg;
	}
	@POST
	@Path("file-upload-md/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Object> fileUploadMd(@PathParam("id") Integer id,
			@MultipartForm FileMdBody fileBody) {
		File file = new File(photoImgPath + id);
		file.mkdirs();
		String newFileName = null;
		try {
			InputStream i = fileBody.inputStream.getBody(InputStream.class, null);
			String[] contentDisposition = fileBody.inputStream.getHeaders().getFirst("Content-Disposition").split(";");
			String fileName = "";
			for (String filename : contentDisposition) {
				if ((filename.trim().startsWith("filename"))) {
					String[] name = filename.split("=");
					fileName = name[1].trim().replaceAll("\"", "");
				}
			}
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			newFileName = BaseUtil.parseDateToStringNomm(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
//			IOUtils.copy(i, new FileOutputStream(new File(photoImgPath + id + "/" + newFileName)));
			
			String srcImageFile = photoImgPath + id + "/" + fileName;
			Files.copy(i, Paths.get(srcImageFile),StandardCopyOption.REPLACE_EXISTING);
			String newFile = photoImgPath + id + "/" + newFileName;
			ImgTools.thumbnail_w_h(srcImageFile, 500, 300, newFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Map<String, Object> msg = new HashMap<String, Object>();
		msg.put("success", 1);
		msg.put("message", "ok");
		msg.put("url", photoImgUrl + id + "/" + newFileName);
		return msg;
	}
	@POST
	@Path("file-upload/{id}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Object> fileUpload(@PathParam("id") Integer id,
			@MultipartForm FileBody fileBody) {
		File file = new File(photoImgPath + id);
		file.mkdirs();
		String newFileName = null;
		try {
			InputStream i = fileBody.inputStream.getBody(InputStream.class, null);
			String[] contentDisposition = fileBody.inputStream.getHeaders().getFirst("Content-Disposition").split(";");
			String fileName = "";
			for (String filename : contentDisposition) {
				if ((filename.trim().startsWith("filename"))) {
					String[] name = filename.split("=");
					fileName = name[1].trim().replaceAll("\"", "");
				}
			}
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			newFileName = BaseUtil.parseDateToStringNomm(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
//			IOUtils.copy(i, new FileOutputStream(new File(photoImgPath + id + "/" + newFileName)));
			
			String srcImageFile = photoImgPath + id + "/" + fileName;
			Files.copy(i, Paths.get(srcImageFile),StandardCopyOption.REPLACE_EXISTING);
			String newFile = photoImgPath + id + "/" + newFileName;
			ImgTools.thumbnail_w_h(srcImageFile, 500, 300, newFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Map<String, Object> msg = new HashMap<String, Object>();
		msg.put("error", 0);
		msg.put("url", photoImgUrl + id + "/" + newFileName);
		return msg;
	}

}
