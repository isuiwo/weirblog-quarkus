package com.weirblog.resource.jpa;

import java.util.Arrays;

import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.weirblog.entity.Tmenu;
import com.weirblog.entity.Users;
import com.weirblog.entity.permission.PermRole;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;

@Path("coreuser")
public class CoreUserResource {

	@GET
	@Path("user/{id}")
	public Uni<Users> getUser(@PathParam("id") Integer id) {
		return Users.findById(id);
	}
	
	@GET
	@Path("user-add")
	@Transactional
	public Uni<Users> userAdd() {
		Users u = new Users();
		u.userName="admin11";
		u.userPwd="admin11";
		PermRole role = new PermRole();
		role.id=8;
		PermRole role2 = new PermRole();
		role2.id=9;
		u.roleList = Arrays.asList(role,role2);
		return u.persist();
	}
	@GET
	@Path("user-update")
	@Transactional
	public Uni<Response> userUpdate() {
		return 
				Panache.withTransaction(()->Users.<Users>findById(3).onItem().ifNotNull().invoke(u->{
						u.email = "21212121212121@qq.com";
						PermRole role = new PermRole();
						role.id=3;
						PermRole role2 = new PermRole();
						role2.id=2;
						u.roleList = Arrays.asList(role,role2);
				}
				)).onItem().ifNotNull()
				.transform(u -> Response.ok(new JsonObject()
						.put("msg", "修改成功")
						.put("success", true)
						.put("data", u)
						).build())
				.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
	}
	@GET
	@Path("role-add")
	@Transactional
	public Uni<Users> roleAdd() {
		PermRole r = new PermRole();
		r.name = "www";
		Tmenu m = new Tmenu();
		m.id=1;
		Tmenu m2 = new Tmenu();
		m2.id=2;
		r.permissions = Arrays.asList(m,m2);
		return r.persistAndFlush();
	}
}
