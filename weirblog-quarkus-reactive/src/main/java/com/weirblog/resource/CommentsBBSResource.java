package com.weirblog.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Context;

import org.hibernate.reactive.mutiny.Mutiny;

import com.weirblog.entity.Comments;
import com.weirblog.entity.CommentsBBS;
import com.weirblog.entity.PostBbs;
import com.weirblog.entity.Users;
import com.weirblog.entity.Video;
import com.weirblog.util.IpUtil;
import com.weirblog.util.JWTUtil;
import com.weirblog.vo.JsonVO;
import com.weirblog.vo.page.QueryResultVO;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.PanacheQuery;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.reactive.ReactiveMailer;
import io.smallrye.mutiny.Uni;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.http.Cookie;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.impl.CookieImpl;
import io.vertx.core.json.JsonObject;
import io.vertx.mutiny.core.Vertx;

/**
 * BBS评论管理
 * 
 * @author weir
 *
 */
@Path("comments-bbs")
@ApplicationScoped
public class CommentsBBSResource {

	@Inject
	ReactiveMailer reactiveMailer;
	
	@GET
	@Path("like/{id}")
	public Uni<Response> like(@PathParam("id") Integer id) {
		return Panache.withTransaction(() -> CommentsBBS.<CommentsBBS>findById(id).onItem().ifNotNull().invoke(entity -> {
			entity.likeCount = entity.likeCount + 1;
		})).onItem().ifNotNull()
				.transform(entity -> Response.ok(new JsonObject().put("msg", "修改成功")
						.put("success", true)
						.put("likeCount", entity.likeCount)
						).build())
				.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
	}
	
	@POST
	@Path("add")
	public Uni<Response> add(CommentsBBS comments,
			@CookieParam("weir-cookie-toekn") String token,
			@Context HttpServletRequest request) {
		comments.ip = IpUtil.getIpAddr(request);
		String username = JWTUtil.getUsername(token);
		return Users.find("token", username).singleResult()
				.chain(u->{
			comments.user = (Users) u;
			return Panache.withTransaction(comments::persist);
		})
				.onItem().ifNotNull().invoke(c->{
					if (comments.commType == 2) {						
						CommentsBBS.update("commentCount = commentCount + 1 where id=?1", comments.pId).subscribeAsCompletionStage().toCompletableFuture();
					}
					PostBbs.update("commentCount = commentCount + 1 where id=?1", comments.postId).subscribeAsCompletionStage().toCompletableFuture();
				})
				.replaceWith(Response
				.ok(new JsonObject().put("msg", "新增成功").put("success", true)).status(Status.OK)::build);
	}

	@POST
	@Path("list")
	public Uni<QueryResultVO<Comments>> list(@QueryParam("page") Integer page, @QueryParam("rows") Integer rows) {
//		System.out.println("----------------" + page + "---" + rows);
		PanacheQuery<Comments> panacheQuery = Comments.findAll().page(page - 1, rows);

		Uni<List<Comments>> comments = panacheQuery.list();
		Uni<Long> count = Comments.count();

		Uni<QueryResultVO<Comments>> pageList = Uni.combine().all().unis(comments, count).combinedWith((list, num) -> {
			QueryResultVO<Comments> qr = new QueryResultVO<>();
			qr.resultList = list;
			qr.totalRecord = num;
			return qr;
		});

		return pageList;
	}

	@POST
	@Path("list2")
	public Uni<QueryResultVO<Comments>> list2(@QueryParam("page") Integer page, @QueryParam("rows") Integer rows) {
		Uni<List<Comments>> panacheQuery = Comments.findAll().page(page - 1, rows).list();
		Uni<Long> count = Comments.count();
		return Uni.combine().all().unis(panacheQuery, count).combinedWith(this::computeFightOutcome);
	}

	private QueryResultVO<Comments> computeFightOutcome(List<Comments> list, Long count) {
		QueryResultVO<Comments> pageList = new QueryResultVO<>();
		pageList.resultList = list;
		pageList.totalRecord = count;
		return pageList;
	}

	@GET
	@Path("{id}")
	public Uni<List<CommentsBBS>> get(@PathParam("id") Integer id) {
		return CommentsBBS.list("pId",id);
	}

}
