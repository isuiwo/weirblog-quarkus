package com.weirblog.resource;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import com.weirblog.entity.Posts;
import com.weirblog.util.BaseUtil;
import com.weirblog.util.ImgTools;
import com.weirblog.vo.DataGrid;
import com.weirblog.vo.FileBody;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import io.quarkus.panache.common.Sort.Direction;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;

/**
 * 文章管理
 * 
 * @author Administrator
 *
 */
@Path("posts")
@ApplicationScoped
public class PostsController {
	@ConfigProperty(name = "photo.img.save-path")
	String photoImgPath;
	@ConfigProperty(name = "photo.img.url-path")
	String photoImgUrl;

	@Location("admin/postAdd.html")
	Template postAdd;
	@Location("admin/postAdd_md.html")
	Template postAddMd;
	@Location("admin/postEdit_md.html")
	Template postEditMd;
	@Location("admin/postShow.html")
	Template postShow;

	@Inject
	Mutiny.Session session;
//	@Inject
//	Mutiny.SessionFactory sessionFactory;

	@GET
	@Path("/get-types")
	public Response getTypes() {
		String singleResult = "";
//				session.createNativeQuery("select GROUP_CONCAT(types) from Posts").getSingleResult()
//				.toString();
		List<String> split = stringSplit(singleResult);
		Map<String, Long> map = split.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		return Response.status(Status.OK).entity(new JsonObject().put("map", map)).build();
	}

	public static List<String> stringSplit(String str) {
		List<String> lis = new ArrayList<>();
		String[] split = str.split(",");
		for (int i = 0; i < split.length; i++) {
			String url = split[i];
			String[] split2 = url.split("，");
			if (split2 != null && split2.length > 1) {
				for (int i2 = 0; i2 < split2.length; i2++) {
					lis.add(split2[i2]);
				}
			} else {
				lis.add(url);
			}
		}
		return lis;
	}

	@POST
	@Path("file-upload/{id}")
	public Response fileUpload(@PathParam("id") Integer id, @MultipartForm FileBody fileBody) {
		File file = new File(photoImgPath + id + "/show");
		file.mkdirs();
		String newFileName = null;
		String newViewPath = null;
		try {
			InputStream i = fileBody.inputStream.getBody(InputStream.class, null);
			String fileName = fileBody.filePath;
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			newFileName = BaseUtil.parseDateToStringNomm(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
			String newFile = photoImgPath + id + "/show/" + newFileName;

			String srcImageFile = photoImgPath + id + "/show/" + fileName;
			Files.copy(i, Paths.get(srcImageFile), StandardCopyOption.REPLACE_EXISTING);
			ImgTools.thumbnail_w_h(srcImageFile, 500, 300, newFile);
			newViewPath = photoImgUrl + id + "/show/" + newFileName;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return Response.status(Status.OK).entity(new JsonObject().put("url", newViewPath)).build();
	}

	@GET
	@Path("addUI")
	@Produces(MediaType.TEXT_HTML)
	public TemplateInstance postAdd() {
		Posts posts = Panache.withTransaction(() -> {
			Uni<Object> singleResult = session.createNativeQuery("select max(id) from Posts").getSingleResult();
			Uni<Posts> posts2 = Uni.createFrom().item(new Posts());
			return Uni.combine().all().unis(singleResult, posts2).combinedWith((sr, p) -> {
				if (sr == null) {
					p.id = 1;
				} else {
					p.id = Integer.valueOf(sr.toString()) + 1;
				}
				return p;
			});
		}).await().indefinitely();
		return postAddMd.data("posts", posts);
	}

	@GET
	@Path("post-edit/{id}")
	@Produces(MediaType.TEXT_HTML)
	public TemplateInstance postEdit(@PathParam("id") Integer id) {
		return postEditMd.data("posts", Posts.findById(id).await().indefinitely());
	}

	@GET
	@Path("post-show/{id}")
	@Produces(MediaType.TEXT_HTML)
	public Uni<String> postShow(@PathParam("id") Integer id) {
		TemplateInstance templateInstance = postShow.data("posts", Posts.findById(id));
		return Uni.createFrom().completionStage(() -> templateInstance.renderAsync());
	}

	@GET
	@Path("pnum/{id}")
	@Transactional
	public Uni<Response> pnum(@PathParam("id") Integer id) {
		return Panache.withTransaction(() -> Posts.<Posts>findById(id).onItem().ifNotNull().invoke(entity -> {
			entity.readNum = entity.readNum + 1;
		})).onItem().ifNotNull()
				.transform(entity -> Response.ok(new JsonObject().put("msg", "修改成功").put("success", true)).build())
				.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
	}

	@POST
	@Path("add")
	public Uni<Response> add(Posts posts) {
		String description = posts.description;
		if (StringUtils.isNotBlank(posts.description) && description.length() > 100) {
			description = description.substring(0, 99);
		}
		posts.description = description;
		return Panache.withTransaction(posts::persist).replaceWith(
				Response.ok(new JsonObject().put("msg", "新增成功").put("success", true)).status(Status.CREATED)::build);
	}

	@POST
	@Path("edit")
	public Uni<Response> edit(Posts posts) {
		return Panache.withTransaction(() -> Posts.<Posts>findById(posts.id).onItem().ifNotNull().invoke(entity -> {
			entity.postTitle = posts.postTitle;
			entity.types = posts.types;
			entity.description = posts.description;
			entity.content = posts.content;
			entity.postPic = posts.postPic;
		})).onItem().ifNotNull()
				.transform(entity -> Response.ok(new JsonObject().put("msg", "修改成功").put("success", true)).build())
				.onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
	}

	@GET
	@Path("list")
	public Uni<Object> list(@QueryParam("page") Integer page, @QueryParam("rows") Integer rows) {
		return Panache.withTransaction(() -> {
			PanacheQuery<Posts> findAll = Posts.findAll(Sort.by("createDate", Direction.Descending));
			Uni<Long> count = findAll.count();
			Uni<List<Posts>> list = findAll.page(Page.of(page - 1, rows)).list();
			return Uni.combine().all().unis(count, list).combinedWith((c, l) -> new DataGrid<Posts>(c, l));
		});
	}

}
