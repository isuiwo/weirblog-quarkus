package com.weirblog.login;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GiteeVo {

	@JsonProperty("client_id")
	public String clientId;
	@JsonProperty("client_secret")
	public String clientSecret;
	@JsonProperty("redirect_uri")
	public String redirectUri;
	public String code;
	@JsonProperty("grant_type")
	public String grantType = "authorization_code";
	public String state = "weir";

	public GiteeVo() {
	}

	public GiteeVo(String clientId, String clientSecret, String redirectUri, String code,String state) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.redirectUri = redirectUri;
		this.code = code;
		this.state = state;
	}

	public GiteeVo(String code, String state) {
		this.code = code;
		this.state = state;
	}

	public GiteeVo(String clientSecret) {
		this.clientSecret = clientSecret;
	}

}
