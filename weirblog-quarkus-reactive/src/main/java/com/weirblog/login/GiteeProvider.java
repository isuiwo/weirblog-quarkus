//package com.weirblog.login;
//
//import javax.ws.rs.client.Client;
//import javax.ws.rs.core.MediaType;
//
//import org.eclipse.microprofile.config.inject.ConfigProperty;
//import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
//
//public class GiteeProvider {
//	@ConfigProperty(name = "gitee.client.id")
//    private String clientId;
//
//	@ConfigProperty(name = "gitee.client.secret")
//    private String clientSecret;
//
//	@ConfigProperty(name = "gitee.redirect.uri")
//    private String redirectUri;
//	
//	private Client client = ResteasyClientBuilder.newClient();
//
//    public String getAccessToken(AccessTokenDTO accessTokenDTO) {
//        MediaType mediaType = MediaType.get("application/json; charset=utf-8");
//        OkHttpClient client = new OkHttpClient();
//
//        RequestBody body = RequestBody.create(mediaType, JSON.toJSONString(accessTokenDTO));
//        String url = "https://gitee.com/oauth/token?grant_type=authorization_code&code=%s&client_id=%s&redirect_uri=%s&client_secret=%s";
//        url = String.format(url, accessTokenDTO.getCode(), clientId, redirectUri, clientSecret);
//        Request request = new Request.Builder()
//                .url(url)
//                .post(body)
//                .build();
//        try (Response response = client.newCall(request).execute()) {
//            String string = response.body().string();
//            JSONObject jsonObject = JSON.parseObject(string);
//            return jsonObject.getString("access_token");
//        } catch (Exception e) {
//            log.error("getAccessToken error,{}", accessTokenDTO, e);
//        }
//        return null;
//    }
//
//
//    public GiteeUser getUser(String accessToken) {
//        OkHttpClient client = new OkHttpClient();
//        Request request = new Request.Builder()
//                .url("https://gitee.com/api/v5/user?access_token=" + accessToken)
//                .build();
//        try {
//            Response response = client.newCall(request).execute();
//            String string = response.body().string();
//            GiteeUser giteeUser = JSON.parseObject(string, GiteeUser.class);
//            return giteeUser;
//        } catch (Exception e) {
//            log.error("getUser error,{}", accessToken, e);
//        }
//        return null;
//    }
//}