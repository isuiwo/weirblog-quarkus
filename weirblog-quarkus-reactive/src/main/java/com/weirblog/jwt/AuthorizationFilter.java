//package com.weirblog.jwt;
//
//import org.apache.commons.lang3.StringUtils;
//import org.eclipse.microprofile.config.inject.ConfigProperty;
//import org.jboss.resteasy.specimpl.RequestImpl;
//
//import io.quarkus.vertx.http.runtime.CurrentVertxRequest;
//import io.vertx.core.http.Cookie;
//import io.vertx.core.json.JsonObject;
//import io.vertx.ext.web.RoutingContext;
//
//import javax.enterprise.inject.spi.CDI;
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerRequestFilter;
//import javax.ws.rs.container.ContainerResponseContext;
//import javax.ws.rs.container.ContainerResponseFilter;
//import javax.ws.rs.core.MultivaluedMap;
//import javax.ws.rs.core.NewCookie;
//import javax.ws.rs.ext.Provider;
//
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//
///**
// *
// * @ClassName: AuthorizationFilter
// * @Description: url权限拦截
// * @author weir
// * @date 2021年8月28日
// *
// */
//@Provider
//public class AuthorizationFilter implements ContainerRequestFilter, ContainerResponseFilter {
//	private volatile CurrentVertxRequest currentVertxRequest;
//
//	CurrentVertxRequest currentVertxRequest() {
//		if (currentVertxRequest == null) {
//			currentVertxRequest = CDI.current().select(CurrentVertxRequest.class).get();
//		}
//		return currentVertxRequest;
//	}
//
//	@Override
//	public void filter(ContainerRequestContext containerRequestContext) {
////		String path = containerRequestContext.getUriInfo().getPath();
////		System.out.println("----------containerRequestContext----path------------" + path);
//		
////		System.out.println("----------containerRequestContext----header------------" + containerRequestContext.getHeaders());
////		RoutingContext httpServerRequest = this.currentVertxRequest().getCurrent();
////        String str = httpServerRequest.getBodyAsString();
////        System.out.println("-----------拦截到请求了--------str---" + str);
////        JsonObject jsonObject = httpServerRequest.getBodyAsJson();
////        System.out.println("-----------拦截到请求了--------jsonObject---" + jsonObject);
////        RequestImpl request = (RequestImpl) containerRequestContext.getRequest();
////        System.out.println("-----------拦截到请求了--------request---" + request);
////        System.out.println("-----------拦截到请求了-----------");
//	}
//
//	@Override
//	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
//			throws IOException {
////		String path = requestContext.getUriInfo().getPath();
////		System.out.println("------responseContext--------path------------" + path);
////		if (path.contains("bbs")) {
////			this.currentVertxRequest().getCurrent().response().addCookie(Cookie.cookie("weir-token", "weiwei"));
////		}
//	}
//
//}