package com.weirblog.vo;

import java.util.List;
import java.util.Map;

import com.weirblog.entity.Posts;
import com.weirblog.vo.page.PageView;

public class IndexVo {

	public PageView<Posts> pageView;
	public String index;
	public List<Posts> rightNews;
	public List<Posts> rightReads;
	public Map<String, Long> mapType;
	@Override
	public String toString() {
		return "IndexVo [pageView=" + pageView + ", index=" + index + ", rightNews=" + rightNews + ", rightReads="
				+ rightReads + ", mapType=" + mapType + "]";
	}
	
}
