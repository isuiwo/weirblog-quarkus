package com.weirblog.vo.bbs;

import java.util.List;

import com.weirblog.entity.PostBbs;
import com.weirblog.entity.Users;
import com.weirblog.vo.page.PageView;

public class IndexBBSVo {

	public List<String> tags;
	public Integer page;
	public String sort;
	public String token;
	public String index;
	public Users user;
	public PostBbs bbsPost;
	
	public PageView<PostBbs> pageView = new PageView<>(1);
}
