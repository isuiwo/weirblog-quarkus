package com.weirblog.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import com.weirblog.entity.permission.PermRole;
import com.weirblog.vo.BaseVo;

/**
 * 菜单资源
 *     
 */
@Entity
public class Tmenu extends BaseVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	public String text;
	public String iconcls;
	public String url;
	public String code;
	
	public Integer step;
	public Integer pid;

	public Integer getPid() {
		return pid;
	}

	public String getCode() {
		return code;
	}

//	@ManyToMany(mappedBy = "permissions")
//	public List<PermRole> roles;
}