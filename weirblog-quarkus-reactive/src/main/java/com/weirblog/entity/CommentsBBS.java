package com.weirblog.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import com.weirblog.vo.BaseVo;

/**
 * BBS评论
 * @author weir
 *
 */
@Entity
public class CommentsBBS extends BaseVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	public Integer pId;
	/**
	 * 文章ID
	 */
	public Integer postId;
	public Integer commentCount = 0;
	/**
	 * 1 根(文章)2叶子(评论回复)
	 */
	public Integer commType;
	@ManyToOne
	@JoinColumn(name = "userId")
	public Users user;
	public Integer likeCount = 0;
	public String ip;
	@CreationTimestamp
	public Date createDate;
	@Column(columnDefinition = "text")
	public String content;
	@Column(columnDefinition = "bit(1) default 0")
	public Boolean approved = true;
	
//	@ManyToOne
//	@JoinColumn(name="pId")
//	public CommentsBBS parent;
//	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "parent")
	@Transient
	public List<CommentsBBS> childs = new ArrayList<>();
}
