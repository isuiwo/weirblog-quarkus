package com.weirblog.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import com.weirblog.vo.BaseVo;

/**
 * 帖子
 * 
 * @author weir
 *
 */
@Entity
public class PostBbs extends BaseVo {

	@Id
	public Integer id;
	@CreationTimestamp
	public LocalDateTime createDate;
	public String title;
//	@Basic(fetch = FetchType.LAZY)
	@Column(columnDefinition = "text")
	public String content;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	public Users user;
	public Integer commentCount = 0;
	public Integer viewCount = 0;
	public Integer likeCount = 0;
	/** 置顶 */
	@Column(columnDefinition = "bit(1) default 0")
	public Boolean sticky = false;
	public String tag;
	
	@Transient
	public List<CommentsBBS> commentsBBSs = new ArrayList<>();
}
