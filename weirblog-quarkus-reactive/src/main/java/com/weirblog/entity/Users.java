package com.weirblog.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.weirblog.entity.permission.PermRole;
import com.weirblog.vo.BaseVo;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class Users extends BaseVo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
    public Date createDate;
    public String email;
    public String userName;
    public String userPwd;
    public String idStr;
    public String profileImageUrl;
    public String accountId;
    public String token;

    @Transient
    public Set<String> roles;
    @Transient
    public String roleIds;
//    @Transient
//    public String token;
    @Transient
    public Set<String> moduleCodes;
    
    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "PermUserRole", joinColumns = {@JoinColumn(name = "userId")}, inverseJoinColumns = {@JoinColumn(name = "roleId")})
    public List<PermRole> roleList;
}
