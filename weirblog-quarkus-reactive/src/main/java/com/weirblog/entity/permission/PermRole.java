package com.weirblog.entity.permission;
import com.weirblog.entity.Tmenu;
import com.weirblog.entity.Users;
import com.weirblog.vo.BaseVo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * 角色表
 * @author weir
 *
 */
@Entity
public class PermRole extends BaseVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    public String name;

    public String remark;
    @Transient
    public String menuIds;
    
    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(name = "PermRolePermission", joinColumns = {@JoinColumn(name = "roleId")}, inverseJoinColumns = {@JoinColumn(name = "permissionId")})
    public List<Tmenu> permissions;

}