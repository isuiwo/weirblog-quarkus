package com.weirblog.entity.permission;

import com.weirblog.vo.BaseVo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 用户角色表
 * @author weir
 *
 */
@Entity
(name = "perm_user_role")
public class PermUserRole extends BaseVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
	@Column(name = "user_id")
    public Integer userId;

	@Column(name = "role_id")
    public Integer roleId;

	public PermUserRole() {
		super();
	}

	public PermUserRole(Integer userId, Integer roleId) {
		super();
		this.userId = userId;
		this.roleId = roleId;
	}

//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}
