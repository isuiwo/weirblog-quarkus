package com.weir.quarku.tanent.resource;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.weir.quarku.tanent.SecurityUtils;
import com.weir.quarku.tanent.entity.SysUser;


@Path("user")
@ApplicationScoped
public class UserResource {

	@RolesAllowed("user_add")
	@GET
	@Path("{id}")
	public SysUser get(@PathParam("id") Integer id) {
		return SysUser.findById(id);
	}
	
	
	@Inject
    SecurityUtils securityUtils;
	
	@GET
	@Path("name")
	public void name() {
		System.out.println("----------securityUtils.getTenantCode()-------" + securityUtils.getTenantCode());
		System.out.println("----------securityUtils.getUsername()-------" + securityUtils.getUsername());
//		System.out.println("----------securityUtils.getClientUniqueId()-------" + securityUtils.getClientUniqueId());
		System.out.println("----------securityUtils.getTenantId()-------" + securityUtils.getTenantId());
	}
}
