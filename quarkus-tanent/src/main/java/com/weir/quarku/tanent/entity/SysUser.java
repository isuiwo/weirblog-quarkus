package com.weir.quarku.tanent.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import java.util.Date;

@Entity
public class SysUser extends PanacheEntityBase {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	public Date createDate;
	public String email;
	public String userName;
	public String userPwd;
	public String idStr;
	public String profileImageUrl;
	public String token;
	@ManyToOne
	@JoinColumn(name = "tenantId")
	public SysTenant tenant;

}
