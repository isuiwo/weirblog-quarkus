package com.weir.quarku.tanent.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * 租户信息和数据库连接
 * @author weir
 *
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "code"))
public class SysTenant extends PanacheEntityBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	public String name;
	public String code;
	
	public Integer acquisitionTimeout;
	public Integer initialSize;
	public String host;
	public String username;
	public String password;
	public Integer maxLifetime;
	public Integer maxSize;
	public Integer minSize;
}
