package com.weir.quarku.tanent;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SaasTenantDbInfo {

    /**
     * https://github.com/quarkusio/quarkus/blob/master/docs/src/main/asciidoc/datasource.adoc
     * <p>
     * io.quarkus.datasource.common.runtime.DatabaseKind
     */
    private String dbKind;

    private String username;

    private String password;

    private String jdbcUrl;

    private Integer minSize = 0;

    private Integer maxSize = 30;

    private Integer initialSize = 0;

    private Integer acquisitionTimeout = 30;

    private Integer leakTimeout = 10;

    private Integer validationTimeout = 6;

    private Integer reapTimeout = 120;
    
    private Integer maxLifetime = 0;
}