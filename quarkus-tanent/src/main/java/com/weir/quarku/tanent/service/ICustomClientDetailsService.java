package com.weir.quarku.tanent.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;

import com.weir.quarku.tanent.entity.SaasUserPrincipal;
import com.weir.quarku.tanent.entity.SysPermission;
import com.weir.quarku.tanent.entity.SysRolesPermission;
import com.weir.quarku.tanent.entity.SysUser;
import com.weir.quarku.tanent.entity.SysUserRole;


@ApplicationScoped
public class ICustomClientDetailsService {

	public SaasUserPrincipal loadClient(String username, String tenantCode) {
		SysUser user = SysUser.find("userName=?1",username).singleResult();
		SaasUserPrincipal saasUserPrincipal = new SaasUserPrincipal();
		saasUserPrincipal.id = user.id;
		saasUserPrincipal.name = user.userName;
		saasUserPrincipal.tenantCode = user.tenant.code;
		saasUserPrincipal.tenantId = user.tenant.id;
		
		List<SysUserRole> userRoles = SysUserRole.list("userId = ?1", user.id);
		List<Integer> roleIds = userRoles.stream().map(SysUserRole::getRoleId).distinct().collect(Collectors.toList());
		
		List<SysRolesPermission> rolesPermissions = SysRolesPermission.list("roleId in (?1)", roleIds);
		List<Integer> permissionIds = rolesPermissions.stream().map(SysRolesPermission::getPermissionId).distinct().collect(Collectors.toList());
		
		List<SysPermission> permissions = SysPermission.list("id in (?1)", permissionIds);
		Set<String> ps = permissions.stream().map(SysPermission::getPermission).distinct().collect(Collectors.toSet());
		
		saasUserPrincipal.permissions = ps;
		return saasUserPrincipal;
	}

}
