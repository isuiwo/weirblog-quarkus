package com.weir.quarku.tanent.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class SysUserRole extends PanacheEntityBase {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	public Integer userId;
	public Integer roleId;
	public Integer getRoleId() {
		return roleId;
	}
}
