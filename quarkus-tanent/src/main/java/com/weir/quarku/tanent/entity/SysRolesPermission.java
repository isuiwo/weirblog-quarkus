package com.weir.quarku.tanent.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class SysRolesPermission extends PanacheEntityBase {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	public Integer permissionId;
	public Integer roleId;
	public Integer getPermissionId() {
		return permissionId;
	}
}
