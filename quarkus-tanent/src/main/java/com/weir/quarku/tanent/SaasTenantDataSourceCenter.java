/**
 * 
 */
package com.weir.quarku.tanent;

import static io.agroal.api.configuration.AgroalConnectionPoolConfiguration.ConnectionValidator.defaultValidator;
import static java.time.Duration.ofSeconds;

import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.ConfigProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import com.mysql.cj.jdbc.Driver;
import com.weir.quarku.tanent.entity.SysTenant;
import com.weir.quarku.tanent.repository.SysTenantRepository;

import io.agroal.api.AgroalDataSource;
import io.agroal.api.configuration.AgroalDataSourceConfiguration;
import io.agroal.api.configuration.supplier.AgroalDataSourceConfigurationSupplier;
import io.agroal.api.security.NamePrincipal;
import io.agroal.api.security.SimplePassword;
import io.quarkus.hibernate.orm.runtime.customized.QuarkusConnectionProvider;
import lombok.extern.slf4j.Slf4j;

/**
 * 这个类是负责构建数据库的配置和租户标识关联和存储类
 * 主要存租户标识, 租户所对应的数据库配置
 * @author xxxxx
 * @since  2021-12-20 11:02
 *
 */
@Slf4j
@ApplicationScoped
public class SaasTenantDataSourceCenter {
//    private final static String POSTGRESQL_JDBC_URL_PREFIX = "jdbc:mysql://";
    private final static String QUARKUS_JDBC_URL = "quarkus.datasource.jdbc.url";
    private final static String QUARKUS_USERNAME = "quarkus.datasource.username";
    private final static String QUARKUS_PWD = "quarkus.datasource.password";

    private final static String QUARKUS_DB_MIN_SIZE = "quarkus.datasource.minSize";
    private final static String QUARKUS_DB_MAX_SIZE = "quarkus.datasource.maxSize";
    private final static String QUARKUS_DB_INITIAL_SIZE = "quarkus.datasource.initialSize";
    private final static String QUARKUS_DB_ACQUISITION_TIMEOUT = "quarkus.datasource.acquisitionTimeout";
    private final static String QUARKUS_DB_LEAK_TIMEOUT = "quarkus.datasource.leakTimeout";
    private final static String QUARKUS_DB_VALIDATION_TIMEOUT = "quarkus.datasource.validationTimeout";
    private final static String QUARKUS_DB_REAP_TIMEOUT = "quarkus.datasource.reapTimeout";
    private final static String QUARKUS_DB_MAX_LIFETIME = "quarkus.datasource.maxLifetime";
    
    /**
     * 租户id和数据源的配置
     */
//    @Inject
//    SysTenantConfigRepository sysTenantConfigRepository;

    @Inject
    SysTenantRepository sysTenantRepository;
    
//    @Inject
//    SysDataSourceRepository sysDatasourceRepository;

    private final ConcurrentHashMap<String, ConnectionProvider> connProviderMap = new ConcurrentHashMap<>();
    
    /**
     * 代码优化
     * @param tenantCode 这个参数名叫tenantCode, 但是实际传值对应我们的租户表的tenantCode字段
     * @return
     * @throws SQLException 
     */
    public ConnectionProvider getConnProvider(String tenantCode) throws SQLException {
    	log.info("--------SaasTenantDataSourceCenter--getConnProvider--tenantCode--" + tenantCode);
        if(connProviderMap.containsKey(tenantCode)) {
           return connProviderMap.get(tenantCode);
        }else {
            //首先这段代码单程序是串行运行的
            synchronized (this.getClass()) {
                //双重判断, 防止第一个进来的线程已经创建了数据源, 第二个进来又创建一个的情况.
                if(connProviderMap.containsKey(tenantCode)) {
                    return connProviderMap.get(tenantCode);
                }else {
                    Optional<SaasTenantDbInfo> dbInfoOfCurrentTenantOp = buildDbInfoFromTenantConfig(tenantCode);
                    SaasTenantDbInfo dbInfoOfCurrentTenant;
                    if(dbInfoOfCurrentTenantOp.isPresent()) {
                        dbInfoOfCurrentTenant = dbInfoOfCurrentTenantOp.get();
                    }else if(tenantCode.equals("base")){
                        dbInfoOfCurrentTenant = buildDefaultDbInfoFromApplicationProperties();
                    }else {
                        log.error("Datasource config is not found for tenant: {}, please config a datasour for it.", tenantCode);
                        throw new RuntimeException(
                                "Datasource config is not found for tenant: " +  tenantCode + ", please config a datasour for it.");
                    }
                    createConnectionProvider(tenantCode, dbInfoOfCurrentTenant);
                    return connProviderMap.get(tenantCode);
                }
            }
        }
    }
    
    private Optional<SaasTenantDbInfo> buildDbInfoFromTenantConfig(String tenantCode) {
    	log.info("--------SaasTenantDataSourceCenter--buildDbInfoFromTenantConfig--tenantCode--" + tenantCode);
//    	log.info("--------SaasTenantDataSourceCenter--buildDbInfoFromTenantConfig--sysTenantRepository--" + sysTenantRepository);
        SysTenant tenant = sysTenantRepository.findByCode(tenantCode);
        if(tenant == null) {
            throw new RuntimeException("When try to build db info for tenant: " + tenantCode + ", tenant is not found");
        }

//        SysTenantConfig config = sysTenantConfigRepository.findByTenantId(tenantOp.id);
//        if(config == null) {
//            log.warn("When try to build db info for tenant: " 
//                    + tenantCode + ", tenant config is not found, will use application.properties");
//            return Optional.empty();
//        }
//        
//        SysDynamicDatasource sysDs = sysDatasourceRepository.findById(config.dynamicDatasourceId);
//        
//        if(sysDs == null) {
//            log.warn("When try to build db info for tenant: " 
//                    + tenantCode + ", datasource config is not found, will use application.properties");
//            return Optional.empty();
//        }
        
        return Optional.of(convertSysDynamicDatasourceToSaasTenantDbInfo(tenant));
    }
    
    /**
     * 将查出来的数据源转换成普通对象
     * @param datasource
     * @return
     */
    private SaasTenantDbInfo convertSysDynamicDatasourceToSaasTenantDbInfo(SysTenant tenant) {
    	SaasTenantDbInfo saasTenantDbInfo = new SaasTenantDbInfo();
        
    	saasTenantDbInfo.setAcquisitionTimeout(tenant.acquisitionTimeout);
    	saasTenantDbInfo.setInitialSize(tenant.initialSize);
    	saasTenantDbInfo.setJdbcUrl(tenant.host);
    	saasTenantDbInfo.setMaxLifetime(tenant.maxLifetime);
    	saasTenantDbInfo.setMaxSize(tenant.maxSize);
    	saasTenantDbInfo.setMinSize(tenant.minSize);
    	saasTenantDbInfo.setPassword(tenant.password);
    	saasTenantDbInfo.setUsername(tenant.username);

        return saasTenantDbInfo;
    }

    public ConcurrentHashMap<String, ConnectionProvider> getConnProviderMap() {
        return connProviderMap;
    }

    
    /**
     * 这个类方法的
     */
    private SaasTenantDbInfo buildDefaultDbInfoFromApplicationProperties() {
    	SaasTenantDbInfo defaultDbInfo = new SaasTenantDbInfo();

        String username = getPropertiesValueByKey(QUARKUS_USERNAME);
        defaultDbInfo.setUsername(username);
        String password = getPropertiesValueByKey(QUARKUS_PWD);
        defaultDbInfo.setPassword(password);
        String jdbcUrl = getPropertiesValueByKey(QUARKUS_JDBC_URL);
        defaultDbInfo.setJdbcUrl(jdbcUrl);
        if(hasKey(QUARKUS_DB_MIN_SIZE)) {
            Integer minSize = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_MIN_SIZE));
            defaultDbInfo.setMinSize(minSize);
        }
        if(hasKey(QUARKUS_DB_MAX_SIZE)) {
            Integer maxSize = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_MAX_SIZE));
            defaultDbInfo.setMaxSize(maxSize);
        }
        if(hasKey(QUARKUS_DB_INITIAL_SIZE)) {
            Integer initialSize = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_INITIAL_SIZE));
            defaultDbInfo.setInitialSize(initialSize);
        }
        if(hasKey(QUARKUS_DB_ACQUISITION_TIMEOUT)) {
            Integer acquisitionTimeout = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_ACQUISITION_TIMEOUT));
            defaultDbInfo.setAcquisitionTimeout(acquisitionTimeout);
        }
        if(hasKey(QUARKUS_DB_LEAK_TIMEOUT)) {
            Integer leakTimeout = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_LEAK_TIMEOUT));
            defaultDbInfo.setLeakTimeout(leakTimeout);
        }

        if(hasKey(QUARKUS_DB_VALIDATION_TIMEOUT)) {
            Integer validationTimeout = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_VALIDATION_TIMEOUT));
            defaultDbInfo.setValidationTimeout(validationTimeout);
        }
        if(hasKey(QUARKUS_DB_REAP_TIMEOUT)) {
            Integer reapTimeout = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_REAP_TIMEOUT));
            defaultDbInfo.setReapTimeout(reapTimeout);
        }
        if(hasKey(QUARKUS_DB_MAX_LIFETIME)) {
            Integer maxLifetime = Integer.parseInt(getPropertiesValueByKey(QUARKUS_DB_MAX_LIFETIME));
            defaultDbInfo.setMaxLifetime(maxLifetime);
        }
        return defaultDbInfo;

    }
    
    private static AgroalDataSourceConfiguration createDataSourceConfiguration(SaasTenantDbInfo dbInfo) {
        // 这里特殊处理一下, 如果配置的JDBC_URL没有POSTGRESQL的前缀, 这里拼接上
//        if(!dbInfo.getJdbcUrl().startsWith(POSTGRESQL_JDBC_URL_PREFIX)) {
//            dbInfo.setJdbcUrl(POSTGRESQL_JDBC_URL_PREFIX + dbInfo.getJdbcUrl());
//        }
        return new AgroalDataSourceConfigurationSupplier()
                .dataSourceImplementation(AgroalDataSourceConfiguration.DataSourceImplementation.AGROAL)
                .metricsEnabled(false)
                .connectionPoolConfiguration(cp -> cp
                        .minSize(dbInfo.getMinSize())
                        .maxSize(dbInfo.getMaxSize())
                        .initialSize(dbInfo.getInitialSize())
                        .connectionValidator(defaultValidator())
                        .acquisitionTimeout(ofSeconds(dbInfo.getAcquisitionTimeout()))
                        .leakTimeout(ofSeconds(dbInfo.getLeakTimeout()))
                        .validationTimeout(ofSeconds(dbInfo.getValidationTimeout()))
                        .reapTimeout(ofSeconds(dbInfo.getReapTimeout()))
                        .connectionFactoryConfiguration(cf -> cf
                                .jdbcUrl(dbInfo.getJdbcUrl())
                                .connectionProviderClass(Driver.class)
                                .principal(new NamePrincipal(dbInfo.getUsername()))
                                .credential(new SimplePassword(dbInfo.getPassword()))
                        )
                ).get();
    }

    public void createConnectionProvider(String tenantCode, SaasTenantDbInfo dbConnectionInfo) throws SQLException {
        AgroalDataSource agroalDataSource = AgroalDataSource.from(createDataSourceConfiguration(dbConnectionInfo));
        QuarkusConnectionProvider quarkusConnectionProvider = new QuarkusConnectionProvider(agroalDataSource);
        this.getConnProviderMap().put(tenantCode, quarkusConnectionProvider);
    }
    
    /**
     * 通过配置文件的key获取value
     * @param key
     * @return
     */
    private static String getPropertiesValueByKey(String key) {
        return ConfigProvider.getConfig().getValue(key, String.class);
    }

    /**
     * 判断是否有key
     * @param key
     * @return
     */
    private static Boolean hasKey(String key) {
        boolean boo = false;
        try {
            String value = ConfigProvider.getConfig().getValue(key, String.class);
            if (value != null) {
                boo = true;
            }
        } catch (Exception e) {
            log.warn("{},will used default value.", e.getMessage());
        }

        return boo;
    }
}
