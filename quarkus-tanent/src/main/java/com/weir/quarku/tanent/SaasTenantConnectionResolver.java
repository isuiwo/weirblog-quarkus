/**
 * 
 */
package com.weir.quarku.tanent;

import java.sql.SQLException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import io.quarkus.arc.Unremovable;
import io.quarkus.hibernate.orm.PersistenceUnitExtension;
import io.quarkus.hibernate.orm.runtime.tenant.TenantConnectionResolver;
import lombok.extern.slf4j.Slf4j;

/**
 * 这个类是配合TenantResolver来实现的.
 * TenantResolver负责拿到租户的标识位,
 * 这个类负责根据标识位来创建数据库连接信息
 * 
 * @author xxxxxx
 * @since  2021-12-20 10:59
 *
 */
@Slf4j
@ApplicationScoped
@PersistenceUnitExtension
@Unremovable
public class SaasTenantConnectionResolver implements TenantConnectionResolver {

    @Inject
    SaasTenantDataSourceCenter saasTenantDataSourceCenter;

    @Override
    public ConnectionProvider resolve(String tenantCode) {
        log.info("------------SaasTenantConnectionResolver--" + tenantCode);
        try {
            return saasTenantDataSourceCenter.getConnProvider(tenantCode);
        } catch (SQLException e) {
            log.error("Get connProvider failed.", e);
            throw new RuntimeException(e);
        }
    }
}
