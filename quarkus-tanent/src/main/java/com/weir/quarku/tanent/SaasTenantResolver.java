/**
 * 
 */
package com.weir.quarku.tanent;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import io.quarkus.arc.Unremovable;
import io.quarkus.hibernate.orm.PersistenceUnitExtension;
import io.quarkus.hibernate.orm.runtime.tenant.TenantResolver;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * 租户ID切换类, 只是切换一个标识, 具体是哪个数据库得看TenantConnectionResolver的实现(SaasTenantConnectionResolver)
 * @author xxxxx
 * @since  2021-12-20 10:52
 *
 */
@Slf4j
@RequestScoped
@Unremovable
//@PersistenceUnitExtension
public class SaasTenantResolver implements TenantResolver {
    
    @Inject
    SecurityUtils securityUtils;

    @Override
    public String getDefaultTenantId() {
        log.info("---------SaasTenantResolver-----getDefaultTenantId--");
        // 默认返回超级租户的标识
        return "w1";
    }

    @Override
    public String resolveTenantId() {
    	log.info("---------SaasTenantResolver-----resolveTenantId--");
        // 这行的目的是从登录的用户拿到租户的标识位
        String tenantCode = securityUtils.getTenantCode();
        log.info("---------SaasTenantResolver-----tenantCode--" + tenantCode);
        return tenantCode;
    }

}
