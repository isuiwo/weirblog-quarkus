package com.weir.quarku.tanent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class SysPermission extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String name;
    @Column(columnDefinition = "enum('menu','button')")   // 资源类型，[menu|button]
    public String resourceType;
    public String url;         // 资源路径
    public String permission;  // 权限字符串,menu例子：role:*，button例子：role:create,role:update,role:delete,role:view
    public Long parentId;      // 父编号
    public String parentIds;   // 父编号列表

	public String getPermission() {
		return permission;
	}
}
