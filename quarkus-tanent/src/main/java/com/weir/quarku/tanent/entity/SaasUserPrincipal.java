package com.weir.quarku.tanent.entity;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户权限信息
 * @author weir
 *
 */
public class SaasUserPrincipal implements Principal {

	public Integer id;
	public Integer tenantId;
	public String tenantCode;
	public String name;
	
    public String token;
    public Set<String> permissions = new HashSet<>(Arrays.asList("admin","user"));
	@Override
	public String toString() {
		return "SaasUserPrincipal [id=" + id + ", tenantId=" + tenantId + ", tenantCode="
				+ tenantCode + ", name=" + name + ", token=" + token + ", permissions=" + permissions
				+ "]";
	}
	@Override
	public String getName() {
		return name;
	}
}
