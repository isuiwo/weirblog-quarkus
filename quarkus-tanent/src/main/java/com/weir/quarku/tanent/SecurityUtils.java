package com.weir.quarku.tanent;

import java.util.Objects;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.weir.quarku.tanent.entity.SaasUserPrincipal;

import io.quarkus.security.identity.SecurityIdentity;
import lombok.extern.slf4j.Slf4j;

/**
 * 安全服务工具类
 *
 * @author xxxxx
 */
@Slf4j
@RequestScoped
public class SecurityUtils {
    
    @Inject
    SecurityIdentity securityIdentity;

    /**
     * 是否是超级管理员
     **/
    public static Boolean isSuperAdmin(Integer userId) {
    	log.info("---------SecurityUtils----------isSuperAdmin--userId--" + userId);
        return Objects.nonNull(userId) && userId == 1;
    }

    public Integer getTenantId() {
        SaasUserPrincipal loginUser = (SaasUserPrincipal)securityIdentity.getPrincipal();
        log.info("---------SecurityUtils----------getTenantId--loginUser--" + loginUser.toString());
        return loginUser.tenantId;
    }

    public String getTenantCode() {
    	SaasUserPrincipal loginUser = (SaasUserPrincipal)securityIdentity.getPrincipal();
    	log.info("---------SecurityUtils----------getTenantCode--loginUser--" + loginUser.toString());
        return loginUser.tenantCode;
    }
    public String getUsername() {
    	SaasUserPrincipal loginUser = (SaasUserPrincipal)securityIdentity.getPrincipal();
    	log.info("---------SecurityUtils----------getTenantCode--loginUser--" + loginUser.toString());
        return loginUser.name;
    }
    
    /**
     * 这个是client 的主键id
     * @return
     */
//    public Integer getClientUniqueId(){
//    	SaasUserPrincipal loginUser = (SaasUserPrincipal)securityIdentity.getPrincipal();
//        return loginUser.clientId;
//    }
}
