package com.weir.quarku.tanent.token;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.quarkus.security.identity.AuthenticationRequestContext;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.identity.SecurityIdentityAugmentor;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;

/**
 * 用来增强SecurityIdentity的内容，比如可以加角色
 * https://quarkus.io/guides/security-customization
 * @author weir
 *
 */
@Slf4j
@ApplicationScoped
public class RolesAugmentor implements SecurityIdentityAugmentor {
	
	@Inject
	SaasSecurityIdentitySupplierManager saasSecurityIdentitySupplierManager;

    @Override
    public Uni<SecurityIdentity> augment(SecurityIdentity identity, AuthenticationRequestContext context) {
    	log.info("---------RolesAugmentor---augment----");
    	saasSecurityIdentitySupplierManager.setIdentity(identity);
        return context.runBlocking(saasSecurityIdentitySupplierManager);
    }

}