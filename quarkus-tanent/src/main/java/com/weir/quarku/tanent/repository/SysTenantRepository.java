package com.weir.quarku.tanent.repository;

import javax.enterprise.context.ApplicationScoped;

import com.weir.quarku.tanent.entity.SysTenant;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class SysTenantRepository {

	public SysTenant findById(Integer tenantId) {
		return SysTenant.findById(tenantId);
	}
	public SysTenant findByCode(String tenantCode) {
		log.info("---------SysTenantRepository-------findByCode-----" + tenantCode);
		SysTenant tenant = SysTenant.find("code", tenantCode).firstResult();
		return tenant;
	}

}
