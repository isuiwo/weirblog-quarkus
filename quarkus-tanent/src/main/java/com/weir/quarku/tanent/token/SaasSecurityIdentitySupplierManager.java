package com.weir.quarku.tanent.token;

import java.util.Optional;
import java.util.function.Supplier;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;
//
//import com.xxxxxx.xxxxxx.admin.security.common.bo.xxxxxxUserPrincipal;
//import com.xxxxxx.xxxxxx.admin.security.service.ICustomClientDetailsService;
//import com.xxxxxx.xxxxxx.admin.security.service.ICustomUserDetailsService;

import com.weir.quarku.tanent.entity.SaasUserPrincipal;
import com.weir.quarku.tanent.service.ICustomClientDetailsService;
import com.weir.quarku.tanent.service.ICustomUserDetailsService;

import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.runtime.QuarkusSecurityIdentity;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import lombok.extern.slf4j.Slf4j;

/**
 * 查询用户身上的角色, 并赋值给Quarkus的SecurityIdentity
 * 目测这个类只会初始化一次.
 * @author xxxxxx
 *
 */
@Slf4j
@Dependent
public class SaasSecurityIdentitySupplierManager implements Supplier<SecurityIdentity> {

    private SecurityIdentity identity;
    
    @Inject
    ICustomUserDetailsService iCustomUserDetailsService;
    
    @Inject
    ICustomClientDetailsService customerClientDetailsService;

    @ActivateRequestContext
    @Override
    public SecurityIdentity get() {
//    	log.info("--------SaasSecurityIdentitySupplierManager---------get------");
//    	log.info("--------SaasSecurityIdentitySupplierManager---------identity.isAnonymous()------" + identity.isAnonymous());
        if (!identity.isAnonymous()) {
            DefaultJWTCallerPrincipal jwtPrincipal = (DefaultJWTCallerPrincipal)identity.getPrincipal();
            String tenantCode = jwtPrincipal.getName();
            log.info("--------SaasSecurityIdentitySupplierManager---------tenantCode------" + tenantCode);
            String username = jwtPrincipal.claim("tenantCode").get().toString();
            log.info("--------SaasSecurityIdentitySupplierManager---------username------" + username);
            //这里判断是不是客户端认证
            Optional<Object> clientCredentialsOp = jwtPrincipal.claim("client_credentials");
            
            if(clientCredentialsOp.isPresent()) {
                try {
                SaasUserPrincipal clientPrincipal = customerClientDetailsService.loadClient(username, tenantCode);
                    return QuarkusSecurityIdentity.builder()
                            .setPrincipal(clientPrincipal)
                            .addAttributes(identity.getAttributes())
                            .addCredentials(identity.getCredentials())
                            .addRoles(identity.getRoles())
                            .build();
                }catch(Exception e) {
                    log.error("Setting quarkus client security identity failed. Caused by:{}", e.getMessage(), e);
                }
            }else {
                try {
                	SaasUserPrincipal userPrincipal = iCustomUserDetailsService.loadUserAccount(username, tenantCode);
                    
                    return QuarkusSecurityIdentity.builder()
                            .setPrincipal(userPrincipal)
                            .addAttributes(identity.getAttributes())
                            .addCredentials(identity.getCredentials())
                            .addRoles(userPrincipal.permissions)
                            .build();
                }catch (Exception e) {
                    log.error("Setting quarkus user security identity failed. Caused by:{}", e.getMessage(), e);
                }
            }
        }
        return identity;
    }

    public void setIdentity(final SecurityIdentity identity) {
        this.identity = identity;
    }
}
