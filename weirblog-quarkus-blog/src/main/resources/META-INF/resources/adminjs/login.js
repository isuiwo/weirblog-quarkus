$(function() {
	/*获取焦点*/
	$('#loginname').textbox('textbox').focus();
	/** 给登录按钮绑定点击事件  */
	$("#login-submit-btn").on("click", function() {
		/** 校验登录参数 ctrl+K */
		var loginname = $("#loginname").val();
		var password = $("#password").val();
		$.ajax({
			url: '/login',
			type: 'post',
			dataType: "json",
			contentType: 'application/json;charset=UTF-8',
			data:
				JSON.stringify({
					"username": loginname,
					"password": password
				}),

			success: function(data) {
				$.cookie("weir-token", data.data.token , { path: '/', expires: 1 });
				window.location.href = "/admin/index/main";
			},
			error: function(jqXHR, textStatus, errorThrown) {
				// alert(jqXHR.responseText);
				// eval("(" + XMLHttpRequest.responseText + ")");
				var errorInfo = JSON.parse(jqXHR.responseText);
				// console.log("-----------------------------"+jqXHR)
				// console.log("------------------jqXHR.responseText-----------"+jqXHR.responseText)
				$.messager.alert("错误信息", errorInfo.message, 'info');
				/*弹出jqXHR对象的信息*/
				// alert(errorInfo.message);
			}

		});
	});

})
function clearForm() {
	$('#formlogin').form('clear');
}
