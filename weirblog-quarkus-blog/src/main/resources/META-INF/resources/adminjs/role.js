var roleDataGrid;
        $(function () {
            roleDataGrid = $('#role_datagrid').datagrid({
            	method: 'get',
                url: '/role/list',
                fit: true,
                fitColumns: true,
                border: false,
                pagination: true,
                idField: 'id',
                pagePosition: 'both',
                checkOnSelect: true,
                selectOnCheck: true,
                pageSize: 5,
                pageList: [5, 10, 20],
                columns: [[{
                    field: 'id',
                    title: '编号',
                    width: 100,
                    checkbox: true
                }, {
                    field: 'name',
                    title: '角色名称',
                    width: 100,
                    sortable: true
                }, {
                    field: 'remark',
                    title: '备注',
                    width: 100,
                    sortable: true
                }]],
                toolbar: [{
                    text: '增加',
                    iconCls: 'ext-icon-add',
                    handler: function () {
                        roleAdd();
                    }
                }, '-', {
                    text: '编辑',
                    iconCls: 'ext-icon-pencil',
                    handler: function () {
                        roleEdit();
                    }
                }, '-', {
                    text: '删除',
                    iconCls: 'ext-icon-pencil_delete',
                    handler: function () {
                        roleDelete();
                    }
                }],
                onRowContextMenu: function (e, rowIndex, rowData) {
                    e.preventDefault();
                    $(this).datagrid('unselectAll');
                    $(this).datagrid('selectRow', rowIndex);
                    $('#role_menu').menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                }
            });

        });

        function roleAdd() {
            var dialog = parent.modalDialog({
                title: '角色添加',
                width: 400,
                height: 500,
                url: '/role/addUI',
                buttons: [{
                    text: '添加',
                    handler: function () {
                        dialog.find('iframe').get(0).contentWindow.roleAdd_submitForm(dialog, roleDataGrid, parent.$);
                    }
                }]
            });
        }

        function roleEdit() {
            var rows = roleDataGrid.datagrid('getChecked');
            if (rows.length == 1) {
                var dialog = parent.modalDialog({
                    title: '角色修改',
                    width: 400,
                    height: 500,
                    url: '/role/editUI/' + rows[0].id,
                    buttons: [{
                        text: '添加',
                        handler: function () {
                            dialog.find('iframe').get(0).contentWindow.roleAdd_submitForm(dialog, roleDataGrid, parent.$);
                        }
                    }]
                });
            } else {
                parent.$.messager.alert('提示', '请选择一条记录进行修改');
            }
        }

//        function editRoleForm() {
//            var rows = roleDataGrid.datagrid('getChecked');
//            if (rows.length == 1) {
//                if ($("#admin_editroleForm").form('validate')) {
//                    $.post('/role/edit', $("#admin_editroleForm").serialize(), function (j) {
//                        if (j.success) {
//                            roleDataGrid.datagrid('load');
//                            $('#admin_editrole').dialog('close');
//                            roleDataGrid.datagrid('uncheckAll');
//                        }
//                        $.messager.show({
//                            title: '提示',
//                            msg: j.msg,
//                            timeout: 5000,
//                            showType: 'slide'
//                        });
//                    }, 'json');
//                }
//            }
//        }

        function roleDelete() {
            var rows = roleDataGrid.datagrid('getChecked');
            var ids = [];
            if (rows.length > 0) {
                $.messager.confirm('确认', '您确认想要删除记录吗？', function (r) {
                    if (r) {
                        for (var i = 0; i < rows.length; i++) {
                            ids.push(rows[i].roleId);
                        }
                        $.post('/role/delete', {roleIds: ids.join(',')}, function (j) {
                            if (j.success) {
                                roleDataGrid.datagrid('load');
                                $('#admin_addrole').dialog('close');
                            }
                            roleDataGrid.datagrid('uncheckAll');
                            $.messager.show({
                                title: '提示',
                                msg: j.msg,
                                timeout: 5000,
                                showType: 'slide'
                            });
                        }, 'json');
                    }
                });
            } else {
                $.messager.show({
                    title: '提示',
                    msg: '请勾选要删除的记录',
                    timeout: 5000,
                    showType: 'slide'
                });
            }
        }