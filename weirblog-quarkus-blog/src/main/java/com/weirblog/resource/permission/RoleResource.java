package com.weirblog.resource.permission;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

import com.weirblog.entity.permission.PermRole;
import com.weirblog.entity.permission.PermRolePermission;
import com.weirblog.vo.DataGrid;
import com.weirblog.vo.JsonVO;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.quarkus.panache.common.Sort.Direction;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;

@Path("role")
public class RoleResource {
	
	@Location("admin/roleAdd.html")
	Template roleAdd;

	@GET
	@Path("list")
	public DataGrid<PermRole> list(@QueryParam("page") Integer page, @QueryParam("rows") Integer rows) {
		PanacheQuery<PermRole> pageQuery = PermRole.findAll(Sort.by("id", Direction.Descending)).page(page-1, rows);
		DataGrid<PermRole> dataGrid = new DataGrid<PermRole>(pageQuery.count(), pageQuery.list());
		return dataGrid;
	}
	
	@GET
	@Path("getAll")
	public List<PermRole> getAll() {
		return PermRole.findAll().list();
	}
	
	@GET
	@Path("addUI")
	@Produces(MediaType.TEXT_HTML)
	public TemplateInstance roleAdd() {
		return roleAdd.data("role", null);
	}
	
	@POST
	@Path("add")
	@Transactional
	public JsonVO<Object> add(
			@FormParam("id") String id,
			@FormParam("name") String name,
			@FormParam("remark") String remark,
			@FormParam("menuIds") String menuIds) {
		JsonVO<Object> json = new JsonVO<>();
		if (StringUtils.isBlank(id)) {
			PermRole role = new PermRole();
			role.name = name;
			role.remark = remark;
			role.persist();
			
			if (StringUtils.isNotBlank(menuIds)) {
				String[] split = StringUtils.split(menuIds, ",");
				for (String str : split) {
					PermRolePermission rolePermission = new PermRolePermission(Integer.valueOf(str), role.id);
					rolePermission.persist();
				}
			}
		}else {
			PermRole v = PermRole.findById(Integer.valueOf(id));
			if (v!=null) {
				v.name = name;
				v.remark = remark;
				
				PermRolePermission.delete("role_id", id);
				
				if (StringUtils.isNotBlank(menuIds)) {
					String[] split = StringUtils.split(menuIds, ",");
					for (String str : split) {
						PermRolePermission rolePermission = new PermRolePermission(Integer.valueOf(str), v.id);
						rolePermission.persist();
					}
				}
			}
		}
		json.setMsg("添加/修改成功");
		json.setSuccess(true);
		return json;
	}
	
	@GET
	@Path("editUI/{id}")
	@Produces(MediaType.TEXT_HTML)
	public TemplateInstance roleEdit(@PathParam("id") Integer id) {
		PermRole role = PermRole.findById(id);
		List<PermRolePermission> list = PermRolePermission.find("role_id", id).list();
		List<Integer> permissionIds = list.stream().map(PermRolePermission::getPermissionId).collect(Collectors.toList());
		role.menuIds = StringUtils.join(permissionIds, ",");
		return roleAdd.data("role", role);
	}
}
