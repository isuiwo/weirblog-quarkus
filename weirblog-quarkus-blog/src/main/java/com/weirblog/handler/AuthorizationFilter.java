//package com.weirblog.handler;
//
//import org.apache.commons.lang3.StringUtils;
//import org.eclipse.microprofile.config.inject.ConfigProperty;
//
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerRequestFilter;
//import javax.ws.rs.ext.Provider;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// *
// * @ClassName: AuthorizationFilter
// * @Description: url权限拦截
// * @author weir
// * @date 2021年8月28日
// *
// */
//@Provider
//public class AuthorizationFilter implements ContainerRequestFilter {
//
//	@ConfigProperty(name = "authorization.permit-patterns")
//	String permitPatterns;
//
//    @Override
//    public void filter(ContainerRequestContext containerRequestContext) {
//    	String[] splits = StringUtils.split(permitPatterns, ",");
//    	List<String> list = Arrays.asList(splits);
//    	String path = containerRequestContext.getUriInfo().getPath();
////    	System.out.println("--------------------path-------------------" + path);
//    	if (list.stream().anyMatch(path::contains)) {
//			return;
//		}
//        Map<String, List<String>> headers = containerRequestContext.getHeaders();
////        List<String> token = headers.get("Authorization");
////        System.out.println("--------------------token-------------------" + token);
//        List<String> cookies = Arrays.asList(headers.get("Cookie").get(0).split(";"));
//        List<String> tokens = cookies.stream().filter(s->s.contains("weir-token")).collect(Collectors.toList());
////        System.out.println("--------------------cookies-------------------" + cookies);
////        System.out.println("--------------------cookies-------------------" + cookies.contains("weir-token"));
//        if (cookies == null || cookies.isEmpty() || tokens == null) {
//        	throw new RuntimeException("No Authorization");
//		}
//
////        try {
////			SysUser user = TokenUtils.getUser(token.get(0));
////			if (user == null) {
////				throw new RuntimeException("非法用户");
////			}
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
//    }
////    @Override
////    public void filter(ContainerRequestContext containerRequestContext) {
////    	String[] splits = StringUtils.split(permitPatterns, ",");
////    	List<String> list = Arrays.asList(splits);
////    	String path = containerRequestContext.getUriInfo().getPath();
////    	if (list.stream().anyMatch(path::contains)) {
////    		return;
////    	}
////    	Map<String, Cookie> cookies = containerRequestContext.getCookies();
////    	Cookie cookieToken = cookies.get("weir-token");
////    	if (cookieToken == null) {
////    		try {
////    			containerRequestContext.setRequestUri(new URI("admin/index"));
////    		} catch (URISyntaxException e) {
////    			e.printStackTrace();
////    		}
////    	}
////    	
////    }
//
//
//}