//package com.weirblog.interceptor;
//
//import java.util.List;
//
//import io.quarkus.arc.deployment.InterceptorBindingRegistrarBuildItem;
//import io.quarkus.arc.processor.InterceptorBindingRegistrar;
//import io.quarkus.arc.processor.InterceptorBindingRegistrar.InterceptorBinding;
//import io.quarkus.arc.runtime.InterceptorBindings;
//import io.quarkus.deployment.annotations.BuildStep;
//
//public class MyExtensionProcessor {
//
//	@BuildStep
//	InterceptorBindingRegistrarBuildItem addInter() {
//		
//		return new InterceptorBindingRegistrarBuildItem(
//				new InterceptorBindingRegistrar() {
//					@Override
//					public List<InterceptorBinding> getAdditionalBindings() {
//						return List.of(InterceptorBinding.of(MyCache.class));
//					}
//				}
//		);
//	}
//}
