package com.weirblog.interceptor;

import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.quarkus.redis.client.RedisClient;
import io.vertx.core.json.Json;
import io.vertx.redis.client.Response;

@Singleton
public class RedisService {

	@Inject
    RedisClient redisClient;

    String get(Object key) {
        Response response = redisClient.get(key.toString());
        if (response == null) {
			return null;
		}
        return response.toString();
    }

    void set(String key, Object value) {
        redisClient.set(Arrays.asList(key, Json.encode(value)));
    }

}
