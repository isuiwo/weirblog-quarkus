package com.weirblog.interceptor;

import io.quarkus.arc.Priority;
import io.quarkus.arc.runtime.InterceptorBindings;
import io.quarkus.logging.Log;
import io.vertx.core.json.Json;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.weirblog.entity.Users;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.*;

@MyCache
@Interceptor
@Priority(Interceptor.Priority.APPLICATION +1)
public class MyCacheInterceptor {

	@Inject
	RedisService redisService;
    @AroundInvoke
    Object execute(InvocationContext context) throws Exception {
    	Log.info("-------------------MyCacheInterceptor-----------");
    	String res = redisService.get("user:2");
    	Log.infov("---------------------redis get : {0}", res);
    	if (res != null) {
			return Json.decodeValue(res, Users.class);
		}
        // 先执行被拦截的方法
        Object rlt = context.proceed();

        // 获取被拦截方法的类名
        String interceptedClass = context.getTarget().getClass().getSimpleName();
        
        Object[] parameters = context.getParameters();
        Log.infov("---------------------{0} parameters : {1}", interceptedClass, Arrays.deepToString(parameters));
        Log.infov("---------------------{0} json parameters : {1}", interceptedClass, Json.encode(parameters));
        
        Parameter[] parameters2 = context.getMethod().getParameters();
        Log.infov("----------------2-----{0} parameters : {1}", interceptedClass, Arrays.deepToString(parameters2));
        // 代码能走到这里，表示被拦截的方法已执行成功，未出现异常
        // 从context中获取通知类型，由于允许重复注解，因此通知类型可能有多个
        List<String> allTypes = getAllTypes(context);

        // 将所有消息类型打印出来
        Log.infov("---------------------{0} allTypes : {1}", interceptedClass, allTypes);
        
        
//        redisService.set(interceptedClass, allTypes);
        // 最后再返回方法执行结果
        Log.infov("---------------------rlt : {0}", rlt);
        if (res == null) {
			redisService.set("user:2", rlt);
//			redisService.set("user:2", Json.encode(rlt));
		}
        return rlt;
    }

    /**
     * 从InvocationContext中取出所有注解，过滤出SendMessage类型的，将它们的type属性放入List中返回
     * @param invocationContext
     * @return
     */
    private List<String> getAllTypes(InvocationContext invocationContext) {
        // 取出所有注解
        Set<Annotation> bindings = InterceptorBindings.getInterceptorBindings(invocationContext);

        List<String> allTypes = new ArrayList<>();

        // 遍历所有注解，过滤出SendMessage类型的
        for (Annotation binding : bindings) {
            if (binding instanceof MyCache) {
               allTypes.add(((MyCache) binding).cacheNames());
               allTypes.add(((MyCache) binding).key());
            }
        }

        return allTypes;
    }

    /**
     * 模拟发送短信
     */
    private void sendSms() {
        Log.info("operating success, from sms");
    }

    /**
     * 模拟发送邮件
     */
    private void sendEmail() {
        Log.info("operating success, from email");
    }
}
