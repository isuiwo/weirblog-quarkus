package com.weirblog.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.weirblog.vo.BaseVo;

import java.util.Date;
import java.util.Set;

@Entity
public class Users extends BaseVo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
    public Date createDate;
    public String email;
    public String userName;
    public String userPwd;
    public String idStr;
    public String profileImageUrl;

    @Transient
    public Set<String> roles;
    @Transient
    public String roleIds;
    @Transient
    public String token;
    @Transient
    public Set<String> moduleCodes;
	@Override
	public String toString() {
		return "Users [id=" + id + ", createDate=" + createDate + ", email=" + email + ", userName=" + userName
				+ ", userPwd=" + userPwd + ", idStr=" + idStr + ", profileImageUrl=" + profileImageUrl + ", roles="
				+ roles + ", roleIds=" + roleIds + ", token=" + token + ", moduleCodes=" + moduleCodes + "]";
	}
    
}
