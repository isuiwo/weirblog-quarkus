package com.weirblog.entity.permission;
import com.weirblog.vo.BaseVo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * 角色表
 * @author weir
 *
 */
@Entity(name = "perm_role")
public class PermRole extends BaseVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    public String name;

    public String remark;
    @Transient
    public String menuIds;

}